package com.zippy.user.ui.activity.otp;


import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.MyOTP;

public interface OTPIView extends MvpView {
    void onSuccess(MyOTP otp);
    void onError(Throwable e);
}
