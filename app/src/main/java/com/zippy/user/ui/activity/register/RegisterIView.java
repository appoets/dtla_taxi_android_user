package com.zippy.user.ui.activity.register;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.MyOTP;
import com.zippy.user.data.network.model.RegisterResponse;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface RegisterIView extends MvpView{
    void onSuccess(RegisterResponse object);
    void onSuccess(Object object);
    void onError(Throwable e);
    void onVerifyEmailError(Throwable e);
    void onSuccess(MyOTP otp);
}
