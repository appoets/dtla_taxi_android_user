package com.zippy.user.ui.activity.wallet;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.AddWallet;

public interface WalletIView extends MvpView {
    void onSuccess(AddWallet object);
    void onError(Throwable e);
}
