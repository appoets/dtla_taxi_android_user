package com.zippy.user.ui.activity.past_trip_detail;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.Datum;

import java.util.List;

public interface PastTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> pastTripDetails);
    void onError(Throwable e);
}
