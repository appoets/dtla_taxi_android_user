package com.zippy.user.ui.activity.passbook;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.WalletResponse;

public interface WalletHistoryIView extends MvpView {
    void onSuccess(WalletResponse response);
    void onError(Throwable e);
}
