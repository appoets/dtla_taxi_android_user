package com.zippy.user.ui.fragment.book_ride;

import com.zippy.user.base.MvpPresenter;

import java.util.HashMap;


public interface BookRideIPresenter<V extends BookRideIView> extends MvpPresenter<V> {
    void rideNow(HashMap<String, Object> obj);
    void getCouponList();
}
