package com.zippy.user.ui.activity.splash;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.User;


public interface SplashIView extends MvpView{
    void onSuccess(User user);
    void onError(Throwable e);
}
