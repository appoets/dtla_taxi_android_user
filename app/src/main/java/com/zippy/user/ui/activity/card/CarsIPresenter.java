package com.zippy.user.ui.activity.card;

import com.zippy.user.base.MvpPresenter;


public interface CarsIPresenter<V extends CardsIView> extends MvpPresenter<V> {
    void card();
}
