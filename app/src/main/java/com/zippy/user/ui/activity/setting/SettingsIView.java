package com.zippy.user.ui.activity.setting;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.AddressResponse;

public interface SettingsIView extends MvpView {

    void onSuccessAddress(Object object);

    void onLanguageChanged(Object object);

    void onSuccess(AddressResponse address);

    void onError(Throwable e);
}
