package com.zippy.user.ui.activity.coupon;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.PromoResponse;

public interface CouponIView extends MvpView {
    void onSuccess(PromoResponse object);
    void onError(Throwable e);
}
