package com.zippy.user.ui.activity.coupon;

import com.zippy.user.base.MvpPresenter;

public interface CouponIPresenter<V extends CouponIView> extends MvpPresenter<V> {
    void coupon();
}
