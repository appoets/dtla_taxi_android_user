package com.zippy.user.ui.fragment.book_ride;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.PromoResponse;


public interface BookRideIView extends MvpView{
    void onSuccess(Object object);
    void onError(Throwable e);
    void onSuccessCoupon(PromoResponse promoResponse);
}
