package com.zippy.user.ui.activity.login;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.ForgotResponse;
import com.zippy.user.data.network.model.Token;

public interface LoginIView extends MvpView{
    void onSuccess(Token token);
    void onSuccess(ForgotResponse object);
    void onError(Throwable e);
}
