package com.zippy.user.ui.activity.social;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.Token;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface SocialIView extends MvpView{
    void onSuccess(Token token);
    void onError(Throwable e);
}
