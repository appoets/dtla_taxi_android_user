package com.zippy.user.ui.activity.wallet;

import com.zippy.user.base.MvpPresenter;
import java.util.HashMap;

public interface WalletIPresenter<V extends WalletIView> extends MvpPresenter<V>{
    void addMoney(HashMap<String, Object> obj);
}
