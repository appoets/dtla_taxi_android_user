package com.zippy.user.ui.activity.upcoming_trip_detail;

import com.zippy.user.base.MvpView;
import com.zippy.user.data.network.model.Datum;

import java.util.List;

public interface UpcomingTripDetailsIView extends MvpView {

    void onSuccess(List<Datum> upcomingTripDetails);
    void onError(Throwable e);
}
