package com.zippy.user.ui.fragment.schedule;

import com.zippy.user.base.MvpView;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface ScheduleIView extends MvpView{
    void onSuccess(Object object);
    void onError(Throwable e);
}
