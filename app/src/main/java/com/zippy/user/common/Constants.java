package com.zippy.user.common;

public interface Constants {

    interface Language {
        String ENGLISH = "en";
        String FRENCH = "fr";
        String FINNISH = "fi";
        String ARABIC = "ar";
    }

    interface MeasurementType {
        String KM = "Kms";
        String MILES = "miles";
    }
}
